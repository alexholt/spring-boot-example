package no.ntnu.idatt2105.l4.demo.repo;

import no.ntnu.idatt2105.l4.demo.model.Meme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Profile("nodb")
@Repository

// Linja under ekskluderer det å konfigurere opp DataSource (som er brukt i forbindelse med JdbcTemplate).
// Grunnen til at vi gjør dette, er at vi skal ikke bruke noen database for "nodb"-profilen, og da vil vi heller
// ikke prøve å konfigurere opp noen datasource. Hvis vi ikke inkluderte linja over, ville vi ha vært nødt til
// å skrive inn connection-string (spring.datasource.url) og brukernavn/passord application-nodb.properties, som virker
// mot sin hensikt når hele poenget med profilen er å kjøre uten database.
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class Lesson4NoDatabaseRepo implements Lesson4Repo {
    Logger logger = LoggerFactory.getLogger(Lesson4Repo.class);

    public Meme saySomething() {

        return new Meme("This meme has never touched the database!", "NEVER!");
    }

    public void create(Meme meme) {

    }

}
