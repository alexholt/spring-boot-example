package no.ntnu.idatt2105.l4.demo.service;

import no.ntnu.idatt2105.l4.demo.model.Meme;
import no.ntnu.idatt2105.l4.demo.repo.Lesson4Repo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class Lesson4Service {

    Logger logger = LoggerFactory.getLogger(Lesson4Service.class);

    // DI av repo her
    @Autowired
    private Lesson4Repo repo;

    public List<Meme> lesson4Message() {
        Meme one = repo.saySomething();
        Meme two = repo.saySomething();

        return new ArrayList<Meme>(Arrays.asList(one, two));
    }

    public void create(Meme meme) {
        repo.create(meme);
    }
}
