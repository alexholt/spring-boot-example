package no.ntnu.idatt2105.l4.demo.service;

public interface SecurityService {

    String createToken(String subject, long ttlMillis);

    String getSubject(String token);

    boolean validToken(String token);
}