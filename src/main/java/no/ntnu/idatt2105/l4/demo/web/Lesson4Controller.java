package no.ntnu.idatt2105.l4.demo.web;

import no.ntnu.idatt2105.l4.demo.aop.TokenRequired;
import no.ntnu.idatt2105.l4.demo.model.Meme;
import no.ntnu.idatt2105.l4.demo.service.Lesson4Service;
import no.ntnu.idatt2105.l4.demo.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin //DO NOTE THIS! This is important for SPA frameworks like Vue!
@RestController
public class Lesson4Controller {

    @Autowired
    private Lesson4Service service;

    @Autowired
    private SecurityService securityService;

    Logger logger = LoggerFactory.getLogger(Lesson4Controller.class);

    @GetMapping("/open")
    public List<Meme> openEndpoint() {

        return this.service.lesson4Message();
    }

    @TokenRequired
    @GetMapping("/restricted")
    public List<Meme> restrictedEndpoint() {
        return this.service.lesson4Message();
    }

    @ResponseBody
    @RequestMapping("/security/generate/token")
    public Map<String, Object> generateToken(@RequestParam(value="subject") String subject){

        String token = securityService.createToken(subject, (15 * 1000 * 60));

        Map<String, Object> map = new LinkedHashMap<>();
        map.put("result", token);

        return map;
    }
}
